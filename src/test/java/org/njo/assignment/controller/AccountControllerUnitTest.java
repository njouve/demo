package org.njo.assignment.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.njo.assignment.model.Account;
import org.njo.assignment.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class AccountControllerUnitTest {

  @InjectMocks
  AccountController accountController;

  @Mock
  AccountService accountService;


  @Mock
  Mapper mapper;

  @Test
  @DisplayName("Get account")
  void testGetAccount() {
    final Account accountMock = new Account();
    accountMock.setId(10000L);

    when(accountService.getAccount(accountMock.getId()))
        .thenReturn(Optional.of(accountMock));

    final ResponseEntity<org.njo.assignment.dto.Account> response = accountController.getAccount(accountMock.getId());
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  @DisplayName("Get missing account")
  void testGetMissingAccount() {
    final Account accountMock = new Account();
    accountMock.setId(10000L);

    when(accountService.getAccount(accountMock.getId()))
        .thenReturn(Optional.empty());

    final ResponseEntity<org.njo.assignment.dto.Account> response = accountController.getAccount(accountMock.getId());
    assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

  }

}
