package org.njo.assignment.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.njo.assignment.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class CustomerControllerUnitTest {

  @InjectMocks
  CustomerController customerController;

  @Mock
  CustomerService customerService;

  @Mock
  Mapper mapper;

  @Test
  @DisplayName("Get missing controller")
  void testGetMissingAccount() {
    when(customerService.getCustomer(1))
        .thenReturn(Optional.empty());

    final ResponseEntity<org.njo.assignment.dto.CustomerDetail> response = customerController.getCustomerDetail(1);
    assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
  }
}
