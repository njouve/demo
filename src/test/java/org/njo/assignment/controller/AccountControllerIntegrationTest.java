package org.njo.assignment.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.njo.assignment.dto.Account;
import org.njo.assignment.dto.AccountCreationParams;
import org.njo.assignment.dto.CustomerDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AccountControllerIntegrationTest {


  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  void newCurrentAccount() {
    final AccountCreationParams creationParams = new AccountCreationParams();
    creationParams.setCustomerId(1);
    creationParams.setInitialCredit(1500);
    final ResponseEntity<Account> response = this.restTemplate.postForEntity("/api/v1/account", creationParams, Account.class);
    Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
    final Account account = response.getBody();

    final ResponseEntity<CustomerDetail> detailCustomerResponse = this.restTemplate.getForEntity("/api/v1/customer/" + creationParams.getCustomerId() + "/detail", CustomerDetail.class);
    Assertions.assertEquals(HttpStatus.OK, detailCustomerResponse.getStatusCode());
    final CustomerDetail detailedCustomer = detailCustomerResponse.getBody();
    assert account != null;
    assert detailedCustomer != null;
    Assertions.assertEquals(detailedCustomer.getCurrentAccount().getId(), account.getId());
    Assertions.assertEquals(detailedCustomer.getCurrentAccount().getBalance(), creationParams.getInitialCredit());
    Assertions.assertEquals(detailedCustomer.getCurrentAccount().getTransactions().iterator().next().getAmount(), creationParams.getInitialCredit());
  }

  @Test
  void newCurrentAccountForInexistingCustomer() {
    final AccountCreationParams creationParams = new AccountCreationParams();
    creationParams.setCustomerId(0);
    creationParams.setInitialCredit(1500);
    final ResponseEntity<Account> response = this.restTemplate.postForEntity("/api/v1/account", creationParams, Account.class);
    Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
  }


}
