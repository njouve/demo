package org.njo.assignment.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.njo.assignment.dto.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CustomerControllerIntegrationTest {


  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  void getExistingCustomer() {
    final Customer customer1 = this.restTemplate.getForObject("/api/v1/customer/1", Customer.class);
    assertThat(customer1.getName()).isEqualTo("Nicolas");
    assertThat(customer1.getSurname()).isEqualTo("Jouve");
  }

  @Test
  void getNonExistingCustomer() {
    final ResponseEntity<Customer> response = this.restTemplate.getForEntity("/api/v1/customer/99999", Customer.class);
    Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
  }
}
