package org.njo.assignment.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.njo.assignment.model.Customer;
import org.njo.assignment.model.CustomerRepository;

@ExtendWith(MockitoExtension.class)
class CustomerServiceUnitTest {

  @InjectMocks
  CustomerServiceImpl customerService;

  @Mock
  CustomerRepository customerRepository;


  @DisplayName("Get Customer by id")
  @Test
  void testGetCustomer() {
    final Customer customer = new Customer();
    customer.setId(1L);
    when(customerRepository.findById(1L))
        .thenReturn(Optional.of(customer));

    Optional<Customer> result = customerService.getCustomer(customer.getId());
    assert result.isPresent();

    assertEquals(customer.getId(), result.get().getId());
  }

  @DisplayName("Get inexisting Customer")
  @Test
  void testGetInexistingCustomer() {

    when(customerRepository.findById(any()))
        .thenReturn(Optional.empty());
    assertTrue(customerService.getCustomer(2).isEmpty());
  }


}
