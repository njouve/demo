package org.njo.assignment.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.njo.assignment.model.Account;
import org.njo.assignment.model.AccountRepository;
import org.njo.assignment.model.Customer;
import org.njo.assignment.service.exception.IllegalAmountException;
import org.njo.assignment.service.exception.NoSuchCustomerException;

@ExtendWith(MockitoExtension.class)
class AccountServiceUnitTest {

  @InjectMocks
  AccountServiceImpl accountService;

  @Mock
  AccountRepository accountRepository;

  @Mock
  CustomerService customerService;

  @Mock
  TransactionService transactionService;

  @DisplayName("Get Account by id")
  @Test
  void testGetAccount() {
    final Account mockAccount = new Account();
    mockAccount.setId(1L);
    when(accountRepository.findById(1L))
        .thenReturn(Optional.of(mockAccount));

    Optional<Account> result = accountService.getAccount(1);

    assert result.isPresent();
    assertEquals(mockAccount.getId(), result.get().getId());
  }

  @Test
  @DisplayName("New account with initial Credit")
  void testNewAccount() {
    final Account newAccountMock = new Account();
    newAccountMock.setId(10000L);

    final Customer mockCustomer = new Customer();
    mockCustomer.setId(100L);
    final long initialCredit = 200;

    when(accountRepository.save(any()))
        .thenReturn(newAccountMock);

    when(customerService.getCustomer(mockCustomer.getId()))
        .thenReturn(Optional.of(mockCustomer));

    Account newAccount = accountService.newAccount(mockCustomer.getId(), initialCredit);

    assertNotNull(newAccount);
    verify(transactionService, times(1)).newTransaction(newAccountMock.getId(), initialCredit);
  }

  @Test
  @DisplayName("New account with no Credit")
  void testNewAccountNoCredit() {
    final Account newAccountMock = new Account();
    newAccountMock.setId(10000L);

    final Customer mockCustomer = new Customer();
    mockCustomer.setId(100L);
    final long initialCredit = 0;

    when(accountRepository.save(any()))
        .thenReturn(newAccountMock);

    when(customerService.getCustomer(mockCustomer.getId()))
        .thenReturn(Optional.of(mockCustomer));

    accountService.newAccount(mockCustomer.getId(), initialCredit);

    verify(transactionService, times(0)).newTransaction(newAccountMock.getId(), initialCredit);
  }

  @DisplayName("Test new account for inexisting customer")
  @Test
  void testNewAccountForInexistingCustomer() {
    when(customerService.getCustomer(anyLong()))
        .thenReturn(Optional.empty());

    assertThrows(NoSuchCustomerException.class,
        () -> accountService.newAccount(12L, 1000));

  }

  @DisplayName("Test new account for illegal amount")
  @Test
  void testNewAccountForIllegalAmount() {
    when(customerService.getCustomer(anyLong()))
        .thenReturn(Optional.of(new Customer()));

    assertThrows(IllegalAmountException.class,
        () -> accountService.newAccount(12L, -100));

  }

}
