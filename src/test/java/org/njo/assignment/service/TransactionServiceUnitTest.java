package org.njo.assignment.service;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.njo.assignment.model.Account;
import org.njo.assignment.model.AccountRepository;
import org.njo.assignment.model.Transaction;
import org.njo.assignment.model.TransactionRepository;
import org.njo.assignment.service.exception.NoSuchAccountException;

@ExtendWith(MockitoExtension.class)
class TransactionServiceUnitTest {

  @InjectMocks
  TransactionServiceImpl transactionService;

  @Mock
  TransactionRepository transactionRepository;

  @Mock
  AccountRepository accountRepository;


  @DisplayName("Test transaction creation")
  @Test
  void testNewTransaction() {
    final Account account = new Account();
    account.setId(99L);
    long amount = 10000L;

    when(transactionRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

    when(accountRepository.findById(account.getId()))
        .thenReturn(Optional.of(account));

    Transaction savedTransaction = transactionService.newTransaction(account.getId(), amount);

    assertEquals(amount, savedTransaction.getAmount());
  }

  @DisplayName("Test transaction creation wit inexisting account")
  @Test
  void testNewTransactionForWrongAccount() {
    when(accountRepository.findById(any()))
        .thenReturn(Optional.empty());

    assertThrows(NoSuchAccountException.class, () -> transactionService.newTransaction(1, 100));

  }


}
