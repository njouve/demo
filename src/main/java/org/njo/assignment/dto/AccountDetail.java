package org.njo.assignment.dto;

import java.util.Collection;

public class AccountDetail extends Account {

  private long balance;


  private Collection<Transaction> transactions;

  public long getBalance() {
    return balance;
  }

  public void setBalance(final long balance) {
    this.balance = balance;
  }

  public Collection<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(final Collection<Transaction> transactions) {
    this.transactions = transactions;
  }

}
