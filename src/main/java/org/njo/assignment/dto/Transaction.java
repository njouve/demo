package org.njo.assignment.dto;

import java.time.LocalDateTime;

public class Transaction {

  private Long id;
  private long amount = 0;
  private LocalDateTime executedAd;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public long getAmount() {
    return amount;
  }

  public void setAmount(final long amount) {
    this.amount = amount;
  }

  @SuppressWarnings("unused")
  public LocalDateTime getExecutedAd() {
    return executedAd;
  }

  public void setExecutedAd(final LocalDateTime executedAd) {
    this.executedAd = executedAd;
  }

}
