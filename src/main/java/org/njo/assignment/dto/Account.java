package org.njo.assignment.dto;


/**
 * A customer account
 */
public class Account {

  private Long id;

  /**
   * @return Id of the account
   */
  public Long getId() {
    return id;
  }

  /**
   *
   */
  public void setId(final Long id) {
    this.id = id;
  }


}
