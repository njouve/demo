package org.njo.assignment.dto;

import java.util.Collection;


/**
 * API facing DTO Class for Customer
 */
public class CustomerDetail extends Customer {

  private Collection<AccountDetail> accounts;
  private AccountDetail currentAccount;

  public AccountDetail getCurrentAccount() {
    return currentAccount;
  }

  public void setCurrentAccount(final AccountDetail currentAccount) {
    this.currentAccount = currentAccount;
  }

  @SuppressWarnings("unused")
  public Collection<AccountDetail> getAccounts() {
    return accounts;
  }

  public void setAccounts(final Collection<AccountDetail> accounts) {
    this.accounts = accounts;
  }


}
