package org.njo.assignment.dto;

/**
 * API facing DTO Class for Customer
 */
public class Customer {

  private Long id;
  private String name;
  private String surname;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(final String surname) {
    this.surname = surname;
  }

}
