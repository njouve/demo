package org.njo.assignment.dto;

public class AccountCreationParams {

  private long initialCredit;
  private long customerId;

  public long getInitialCredit() {
    return initialCredit;
  }

  public void setInitialCredit(final long initialCredit) {
    this.initialCredit = initialCredit;
  }

  public long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(final long customerId) {
    this.customerId = customerId;
  }

}
