package org.njo.assignment.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import java.util.Collection;

@Entity
public class Account {

  @Id()
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;


  @ManyToOne(targetEntity = Customer.class)
  @JoinColumn(name = "customer_id", nullable = false)
  @NotNull
  private Customer customer;
  @OneToMany(mappedBy = "account")
  private Collection<Transaction> transactions;

  public long getBalance() {
    return transactions != null ? transactions.stream().mapToLong(Transaction::getAmount).sum() : 0;
  }

  public Collection<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(final Collection<Transaction> transactions) {
    this.transactions = transactions;
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }


  /**
   * @return customer to which the account belong
   */
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(final Customer customer) {
    this.customer = customer;
  }

}
