package org.njo.assignment.model;

import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import java.util.Collection;

@Entity
public class Customer {

  @Id()
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private String surname;

  @OneToMany(mappedBy = "customer")
  private Collection<Account> accounts;

  @OneToOne()
  @Nullable
  private Account currentAccount;

  public Collection<Account> getAccounts() {
    return accounts;
  }

  @SuppressWarnings("unused")
  public void setAccounts(final Collection<Account> accounts) {
    this.accounts = accounts;
  }

  public Account getCurrentAccount() {
    return currentAccount;
  }

  public void setCurrentAccount(final Account currentAccount) {
    this.currentAccount = currentAccount;
  }


  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  @SuppressWarnings("unused")
  public void setSurname(final String surname) {
    this.surname = surname;
  }
}
