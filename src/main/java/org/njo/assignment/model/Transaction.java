package org.njo.assignment.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
public class Transaction {

  @Id()
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private long amount = 0;

  private Timestamp executedAd;
  @ManyToOne(targetEntity = Account.class)
  @JoinColumn(name = "account_id", nullable = false)
  @NotNull
  @SuppressWarnings("unresolved")
  private Account account;

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public long getAmount() {
    return amount;
  }

  public void setAmount(final long amount) {
    this.amount = amount;
  }

  public Timestamp getExecutedAd() {
    return executedAd;
  }

  public void setExecutedAd(final Timestamp executedAd) {
    this.executedAd = executedAd;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(final Account account) {
    this.account = account;
  }


}
