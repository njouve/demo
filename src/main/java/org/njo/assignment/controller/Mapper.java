package org.njo.assignment.controller;


import org.njo.assignment.dto.Account;
import org.njo.assignment.dto.AccountDetail;
import org.njo.assignment.dto.Customer;
import org.njo.assignment.dto.CustomerDetail;
import org.njo.assignment.dto.Transaction;

@org.mapstruct.Mapper(
    componentModel = "spring"
)
public interface Mapper {

  Customer customerToDTO(org.njo.assignment.model.Customer customer);

  CustomerDetail customerToDetailDTO(org.njo.assignment.model.Customer customer);

  AccountDetail accountToDetailDTO(org.njo.assignment.model.Account account);

  Account accountToDTO(org.njo.assignment.model.Account account);

  @SuppressWarnings("unused")
  Transaction transactiontoDTO(org.njo.assignment.model.Transaction transaction);


}
