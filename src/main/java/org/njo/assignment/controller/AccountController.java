package org.njo.assignment.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Optional;
import org.njo.assignment.dto.Account;
import org.njo.assignment.dto.AccountCreationParams;
import org.njo.assignment.dto.Customer;
import org.njo.assignment.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${api.path}/${api.version}/account")
@Tag(name = "Account management", description = "Manages accounts")
public class AccountController {

  private final AccountService accountService;

  private final Mapper mapper;
  private final Logger logger = LoggerFactory.getLogger(AccountController.class);

  @Autowired
  public AccountController(final AccountService accountService, final Mapper mapper) {
    this.accountService = accountService;
    this.mapper = mapper;
  }

  @GetMapping("/{id}")
  @Operation(summary = "Get Account by Id")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "404", description = "Inexisting account", content = @Content),
      @ApiResponse(responseCode = "200", description = "Existing account", content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Customer.class)))
  })
  public ResponseEntity<Account> getAccount(@PathVariable final long id) {
    logger.info("Getting account {}", id);
    final Optional<org.njo.assignment.model.Account> account = accountService.getAccount(id);
    return account.map(value -> new ResponseEntity<>(
        mapper.accountToDTO(value),
        HttpStatus.OK
    )).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping()
  @Operation(summary = "Create new currentAccount for specified customer and initial credit")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "404", description = "Inexisting Customer", content = @Content),
      @ApiResponse(responseCode = "201", description = "Account was created", content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Account.class)))
  })
  public ResponseEntity<Account> newCurrentAccount(@RequestBody final AccountCreationParams accountCreationParams) {
    logger.info("New current Account for customerId: {} and initialCredit: {}", accountCreationParams.getCustomerId(), accountCreationParams.getInitialCredit());
    final org.njo.assignment.model.Account createdAccount = accountService.newAccount(accountCreationParams.getCustomerId(), accountCreationParams.getInitialCredit());
    return new ResponseEntity<>(
        mapper.accountToDTO(createdAccount),
        HttpStatus.CREATED
    );
  }

}
