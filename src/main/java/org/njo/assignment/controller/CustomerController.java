package org.njo.assignment.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Optional;
import org.njo.assignment.dto.Customer;
import org.njo.assignment.dto.CustomerDetail;
import org.njo.assignment.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${api.path}/${api.version}/customer")
@Tag(description = "Get customer and customer detail", name = "Customer management")
public class CustomerController {

  private final CustomerService customerService;
  private final Mapper mapper;

  private final Logger logger = LoggerFactory.getLogger(CustomerController.class);

  @Autowired
  public CustomerController(final CustomerService customerService, final Mapper mapper) {
    this.mapper = mapper;
    this.customerService = customerService;

  }

  @GetMapping("/{id}")
  @Operation(summary = "Get customer summary by Id")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "404", description = "Inexisting Customer", content = @Content),
      @ApiResponse(responseCode = "200", description = "Existing customer", content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Customer.class)))
  })
  public ResponseEntity<Customer> getCustomer(@PathVariable final long id) {
    logger.info("Getting customer for {}", id);
    final Optional<org.njo.assignment.model.Customer> customer = customerService.getCustomer(id);
    return customer.map(value -> new ResponseEntity<>(
        mapper.customerToDTO(value),
        HttpStatus.OK
    )).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @GetMapping("/{id}/detail")
  @Operation(summary = "Get customer detail by Id")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "404", description = "Inexisting Customer", content = @Content),
      @ApiResponse(responseCode = "200", description = "Existing customer", content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = CustomerDetail.class)))
  })
  public ResponseEntity<CustomerDetail> getCustomerDetail(@PathVariable final long id) {
    logger.info("Getting customer detail for {}", id);
    final Optional<org.njo.assignment.model.Customer> customer = customerService.getCustomer(id);
    return customer.map(value -> new ResponseEntity<>(
        mapper.customerToDetailDTO(value),
        HttpStatus.OK
    )).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }
}
