package org.njo.assignment.service;

import java.util.Optional;
import org.njo.assignment.model.Customer;
import org.njo.assignment.model.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Impl. for customer management
 */
@Service
public class CustomerServiceImpl implements CustomerService {

  private final CustomerRepository customerRepository;

  @Autowired
  public CustomerServiceImpl(final CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }

  @Override
  public Optional<Customer> getCustomer(final long id) {
    return customerRepository.findById(id);
  }
}
