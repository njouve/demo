package org.njo.assignment.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchAccountException extends RuntimeException {

  public NoSuchAccountException(final String message) {
    super(message);
  }
}
