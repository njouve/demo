package org.njo.assignment.service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Optional;
import org.njo.assignment.model.Account;
import org.njo.assignment.model.AccountRepository;
import org.njo.assignment.model.Transaction;
import org.njo.assignment.model.TransactionRepository;
import org.njo.assignment.service.exception.NoSuchAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Impl. for transaction management
 */
@Service
public class TransactionServiceImpl implements TransactionService {

  private final TransactionRepository transactionRepository;
  private final AccountRepository accountRepository;

  @Autowired
  public TransactionServiceImpl(final TransactionRepository transactionRepository, final AccountRepository accountRepository) {
    this.transactionRepository = transactionRepository;
    this.accountRepository = accountRepository;
  }

  @Override
  public Transaction newTransaction(final long accountId, final long amount) {
    final Optional<Account> account = accountRepository.findById(accountId);
    if (account.isEmpty()) {
      throw new NoSuchAccountException("No account with this id :" + accountId);
    }
    Transaction newTransaction = new Transaction();
    newTransaction.setExecutedAd(Timestamp.from(Instant.now()));
    newTransaction.setAccount(account.get());
    newTransaction.setAmount(amount);
    newTransaction = transactionRepository.save(newTransaction);
    if (account.get().getTransactions() == null) {
      account.get().setTransactions(new ArrayList<>());
    }
    account.get().getTransactions().add(newTransaction);
    return newTransaction;
  }
}
