package org.njo.assignment.service;

import java.util.Optional;
import org.njo.assignment.model.Customer;

/**
 * Interface for customer management
 */
public interface CustomerService {

  /**
   * Simple finder for customer by id
   */
  Optional<Customer> getCustomer(long id);

}
