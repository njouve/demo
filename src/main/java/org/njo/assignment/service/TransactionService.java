package org.njo.assignment.service;

import org.njo.assignment.model.Transaction;

/**
 * Interface for transaction management
 */
public interface TransactionService {

  Transaction newTransaction(long accountId, long amount);

}
