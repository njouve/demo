package org.njo.assignment.service;

import java.util.Optional;
import org.njo.assignment.model.Account;

/**
 * Interface for account management
 */
public interface AccountService {

  Optional<Account> getAccount(long id);

  Account newAccount(long customerId, long initialCredit);
}
