package org.njo.assignment.service;

import jakarta.transaction.Transactional;
import java.util.Optional;
import org.njo.assignment.model.Account;
import org.njo.assignment.model.AccountRepository;
import org.njo.assignment.model.Customer;
import org.njo.assignment.service.exception.IllegalAmountException;
import org.njo.assignment.service.exception.NoSuchCustomerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Impl. for account management
 */
@Service
public class AccountServiceImpl implements AccountService {

  private final AccountRepository accountRepository;
  private final CustomerService customerService;

  private final TransactionService transactionService;

  @Autowired
  public AccountServiceImpl(final AccountRepository accountRepository, final CustomerService customerService, final TransactionService transactionService) {
    this.accountRepository = accountRepository;
    this.customerService = customerService;
    this.transactionService = transactionService;
  }

  @Override
  public Optional<Account> getAccount(final long id) {
    return accountRepository.findById(id);
  }

  @Override
  @Transactional
  public Account newAccount(final long customerId, final long initialCredit) {
    final Optional<Customer> customer = customerService.getCustomer(customerId);
    if (customer.isEmpty()) {
      throw new NoSuchCustomerException("No customer with id " + customerId);
    }
    if (initialCredit < 0) {
      throw new IllegalAmountException("Cannot create account withe negative initial credit  " + initialCredit);
    }
    Account account = new Account();
    account.setCustomer(customer.get());
    customer.get().setCurrentAccount(account);
    account = accountRepository.save(account);
    customer.get().setCurrentAccount(account);

    if (initialCredit > 0) {
      transactionService.newTransaction(account.getId(), initialCredit);
    }
    return account;
  }
}
