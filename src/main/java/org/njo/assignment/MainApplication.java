package org.njo.assignment;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.SpringApplicationEvent;
import org.springframework.context.event.EventListener;


/**
 * Entry point to the App.
 */
@SpringBootApplication
public class MainApplication {

  public static void main(final String[] args) {
    SpringApplication.run(MainApplication.class, args);
  }

  @EventListener(classes = {ApplicationReadyEvent.class})
  public void displayWelcomeUrlOnStartup(final SpringApplicationEvent ignoredEvent) {
    LoggerFactory.getLogger(MainApplication.class).info("Please open http://localhost:8080/");
  }
}
