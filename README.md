# NIJ Assignment Project

## Main used technos

- Spring Boot
- JPA/Hibernate
- H2 In Memory Database
- Swagger Frontend

## CI/CD

- simple CI/CD .gitlab-ci.yml is used
- pipeline on tags update pages ( see )

## Metrics and info

- [Site info](https://njouve.gitlab.io/demo)
- [Code coverage](https://njouve.gitlab.io/demo/jacoco)
- [SONAR Metrics](https://sonarcloud.io/project/overview?id=njouve_demo)

## Development requirements

- JDK 20
- Maven

## Instructions for dev

### Build

```mvn package```

### Test

```mvn test```

### Run

```mvn spring-boot:run```

### Test with docker

- First build and launch docker image:

```docker build -t njo_assignment:1 . && docker run -p 8080:8080 -it njo_assignment:1 ```

- Next open http://localhost:8080/
